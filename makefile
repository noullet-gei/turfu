# makefile pour MinGW

CCOPT = -Wall -O2
C_SRC = model.c nb_serial.c appli.c
CPP_SRC =
EXE = turfu.exe

OBJS = $(C_SRC:.c=.o) $(CPP_SRC:.cpp=.o)

# linkage
$(EXE) : $(OBJS)
	g++ -o $(EXE) $(OBJS) -lgdi32 -lopengl32

# compilage
.c.o :
	gcc $(CCOPT) -c $<

.cpp.o :
	g++ $(CCOPT) -c $<
# other

clean :
	rm *.o; rm *.exe

# dependances : 
model.c : model.h images.c
appli.c : model.h nb_serial.h
