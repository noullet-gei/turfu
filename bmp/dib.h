#ifndef IMPARS_TYPE
#include "impars.h"
#endif

void BMPreadHeader( impars *s, int hand );
void BMPwriteHeader24( impars *d, int hand );
int BMPreadLineIntoRGB( impars *s, int hand, unsigned char *rgbLine );
int BMPwriteLineFromRGB( impars *d, int hand, unsigned char *rgbLine );
void BMPwriteBitsLine( impars *d, int hand, char *rbitbuf, char *vbitbuf, char *bbitbuf );
