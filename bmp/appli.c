/* ce programme est pour convertir des images bmp en buffers appropries pour otexture opengl
   et coder ces buffers en C pour inclusion dans le code source d'une application */

#include <windows.h>			/* must include this before GL/gl.h */
#include <GL/gl.h>			/* OpenGL header file */
#include <stdio.h>


/* ============= texture from BMP file ======================= */

#include "dib.h"
#include <fcntl.h>
#include <io.h>

#include <stdarg.h>

/* gasp pour windows */

void gasp( char *fmt, ... )  /* fatal error handling */
{
  char lbuf[1024];
  va_list  argptr;
  va_start( argptr, fmt );
  vsprintf( lbuf, fmt, argptr );
  va_end( argptr );
  MessageBox(NULL, lbuf, "HORREUR", MB_OK);
  exit(1);
}


typedef struct
{
unsigned char * rgbabuf;
int W, H;
} imbuf;

void alloc_image_rgba( imbuf * img )
{
img->rgbabuf = malloc( img->W * img->H * 4 );
if ( img->rgbabuf == NULL ) 
   gasp("erreur malloc");
}   

void read_bmp_rgba( imbuf * img, char * fnam )
{
int srchand, x, y, i; unsigned char *linbuf, *p;
impars s;
srchand = open( fnam, O_RDONLY | O_BINARY );       /*DOS*/
if ( srchand <= 0 )  gasp("%s non ouvert\n", fnam );

BMPreadHeader( &s, srchand );
img->W = s.wi; img->H = s.he;
alloc_image_rgba( img );
linbuf = malloc( 4 * s.ll );
if ( linbuf == NULL ) 
   gasp("erreur malloc");
/* N.B. la texture a Y min en bas, comme BMP (au contraire
   des autres formats d'image )
 */
for ( y = 0; y < s.he; y++ )
    {
    read( srchand, linbuf, s.ll );
    i = 0; p = img->rgbabuf + y * 4 * s.wi;
    for ( x = 0; x < s.wi; x++ )
        {
        *(p++) = linbuf[i+2];  /* R */
        *(p++) = linbuf[i+1];  /* G */
        *(p++) = linbuf[i];    /* B */
        *(p++) = 255;          /* A */
        i += 3;
        }
    }
close( srchand ); free( linbuf );
}

// ici on a du rgba dans img[i].rgbabuf, 32 bits/pixel
// a convertir en C
void rgba2c( imbuf * img, int index )
{
printf( "int W%d = %d;\n", index, img->W );
printf( "int H%d = %d;\n", index, img->H );
printf( "unsigned int RGBAbuf%d[] = {\n", index );
unsigned int i;
for	( i = 0; i < ( img->W * img->H ); ++i )
	{
	printf( "0x%08x,", *(((unsigned int *)img->rgbabuf)+i) );
	if	( ( i & 15 ) == 15 )
		printf("\n");
	}
printf("};\n");
}

int main()
{
int i;

imbuf img[10];
char fnambuf[32];

for	( i = 0; i < 10; ++i )
	{
	snprintf( fnambuf, sizeof(fnambuf), "%d.bmp", i );
	read_bmp_rgba( &img[i], fnambuf );
	// ici on a du rgba dans img[i].rgbabuf, 32 bits/pixel
	rgba2c( &img[i], i );
	}
}

