#define IMPARS_TYPE

typedef struct /* structure repres.tottant les parametres d'une image ... */
{
int wi, he; /* dimensions utiles en pixels */
int x0, y0; /* offset fenetre */
int dx, dy; /* dimensions fenetre */
int ll;     /* encombrement d'une ligne dans la base de donnees, en bytes */
long tot;   /* encombrement total des pixels */
int bppx;   /* bits par pixel */
int hdlen;  /* header length */
int maplen; /* colormap length (bytes) */
} impars;
