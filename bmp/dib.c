#include <stdio.h>
#include <fcntl.h>
#include <io.h>

#include "dib.h"

/*
gcc -c -o dib.o dib.c
*/

void gasp( char *lbuf, ... );

/* -------------------- BMP specific stuff --------------------------- */

void BMPreadHeader( impars *s, int hand )
{
unsigned char buf[64];
read( hand, buf, 54 );
if (  ( buf[0] != 0x42 ) || ( buf[1] != 0x4D )  )
   gasp("not a BMP file");
s->wi = buf[18] + ( buf[19] << 8 );
s->he = buf[22] + ( buf[23] << 8 );
s->bppx = buf[28];
// printf("%d bits par pixels, w=%d, l=%d\n", s->bppx, s->wi, s->he );
if   ( s->bppx != 24 ) gasp("only 24 bits/pix is supported");
s->hdlen = buf[14];
if ( s->hdlen != 40 ) gasp("header length != 40");
if ( buf[30] != 0 )  gasp("only uncompressed is supported");
s->tot = buf[34] + ( buf[35] << 8 ) + ( buf[36] << 16 ) + ( buf[37] << 24 );
// printf("encombrement des pixels de l'image source = %d\n", s->tot);
s->ll = s->tot / s->he;
// printf("encombrement d'une ligne du fichier source = %d\n", s->ll);
if ( s->tot != ( s->he * s->ll ) ) gasp("pb longueur totale");
}

void BMPwriteHeader24( impars *d, int hand )
{
unsigned char buf[64]; int i;
for ( i = 0; i < 54; i++ ) buf[i] = 0;

d->bppx = 24;
d->hdlen = 54;
d->ll = d->wi * 3;
i =  ( d->ll & 3 );        /* residu division par 4 */
if (i) d->ll += ( 4 - i ); /* maintenant ll est divisible par 4 */
d->tot = d->ll * d->he;

buf[0] = 0x42; buf[1] = 0x4D;
i = d->tot + d->hdlen;
buf[2] = i;
buf[3] = i >> 8;
buf[4] = i >> 16;
buf[5] = i >> 24;

buf[10] = d->hdlen;

buf[14] = d->hdlen - 14;

buf[18] = d->wi;
buf[19] = d->wi >> 8;
buf[22] = d->he;
buf[23] = d->he >> 8;
buf[26] = 1;
buf[28] = d->bppx;
  
buf[34] = d->tot;
buf[35] = d->tot >> 8;
buf[36] = d->tot >> 16;
buf[37] = d->tot >> 24;
buf[38] = 0x6D; buf[39] = 0x0B;
buf[42] = 0x6D; buf[43] = 0x0B;
write( hand, buf, 54 );
}

int BMPreadLineIntoRGB( impars *s, int hand, unsigned char *rgbLine )
{
register int xs, is, id; 
static char buf[3*4096];
/*
 * ATTENTION : on lit en partant de la fin de la ligne pour :
 * 1) permuter gauche et droite pour corriger le fait que BMP commence en bas
 * 1) permuter R et B
 */
read( hand, buf, s->ll );
id = 0; is = ( s->x0 + s->dx ) * 3 - 1;
for ( xs = s->x0; xs < ( s->x0 + s->dx ); xs++ )
    {
    rgbLine[id++] = buf[is--];
    rgbLine[id++] = buf[is--];
    rgbLine[id++] = buf[is--];
    };
return( id );
}

int BMPwriteLineFromRGB( impars *d, int hand, unsigned char *rgbLine )
{
register int xd, is, id; 
static char buf[3*4096];
id = 0; is = ( d->wi ) * 3 - 1;
for ( xd = 0; xd < d->wi; xd++ )
    {
    buf[id++] = rgbLine[is--];
    buf[id++] = rgbLine[is--];
    buf[id++] = rgbLine[is--];
    };
return ( write( hand, buf, d->ll ) );
}

void BMPwriteBitsLine( impars *d, int hand, char *rbitbuf, char *vbitbuf, char *bbitbuf )
{
register int xd, is, id; 
static char buf[3*4096];
id = 0; is = d->wi  - 1;
for ( xd = 0; xd < d->wi; xd++ )
    {
    buf[id++] = ( bbitbuf[is] )?(255):(0);
    buf[id++] = ( vbitbuf[is] )?(255):(0);
    buf[id++] = ( rbitbuf[is] )?(255):(0);
    is--;
    };
write( hand, buf, d->ll );

}




