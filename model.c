/* objet, ici une image de chiffre de 0 a 9 */

#include <windows.h>			/* must include this before GL/gl.h */
#include <GL/gl.h>			/* OpenGL header file */
#include <stdio.h>

/* ================== creation texture ===================== */

int gloTexW, gloTexH;	/* dim. texture pour resize "pixel acurate"  */

/* ===================== donnees globales ======================== */

// ce fichier doit etre genere a partir des images bmp avec le prog bmp/bmp2c > images.c
#include "images.c"

static GLuint texHand[10];	// 10 textures
static float xyz3[3*9]; /* points dans l'espace 3d */
static float st2[2*9];	 /* coord de texture s, t */
static unsigned vx[4*4]; /* tableau servant a ordonner les vertices */
static unsigned char * rgbabuf[10];	// pointeurs sur les data de images.c

/* ======================= objets ================================ */

/* un panneau de 1 quad

	a1---a2
	|     |
	a0---a3
 */
void initModel( int * texW, int * texH )
{
int i=0;
xyz3[i++] = 0.0f; xyz3[i++] = 0.0f; xyz3[i++] = 0.0f;
xyz3[i++] = 0.0f; xyz3[i++] = 2.0f; xyz3[i++] = 0.0f;
xyz3[i++] = 2.0f; xyz3[i++] = 2.0f; xyz3[i++] = 0.0f;
xyz3[i++] = 2.0f; xyz3[i++] = 0.0f; xyz3[i++] = 0.0f;
i = 0;
st2[i++] = 0.0f; st2[i++] = 0.0f; 
st2[i++] = 0.0f; st2[i++] = 1.0f; 
st2[i++] = 1.0f; st2[i++] = 1.0f; 
st2[i++] = 1.0f; st2[i++] = 0.0f; 
i = 0;
vx[i++] = 0;
vx[i++] = 1;
vx[i++] = 2;
vx[i++] = 3;

rgbabuf[0] = (unsigned char *)RGBAbuf0;
rgbabuf[1] = (unsigned char *)RGBAbuf1;
rgbabuf[2] = (unsigned char *)RGBAbuf2;
rgbabuf[3] = (unsigned char *)RGBAbuf3;
rgbabuf[4] = (unsigned char *)RGBAbuf4;
rgbabuf[5] = (unsigned char *)RGBAbuf5;
rgbabuf[6] = (unsigned char *)RGBAbuf6;
rgbabuf[7] = (unsigned char *)RGBAbuf7;
rgbabuf[8] = (unsigned char *)RGBAbuf8;
rgbabuf[9] = (unsigned char *)RGBAbuf9;

for	( i = 0; i < 10; ++i )
	{
	glGenTextures( 1, &texHand[i] );
	glBindTexture( GL_TEXTURE_2D, texHand[i] );

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, W0, H0, 
		0, GL_RGBA, GL_UNSIGNED_BYTE, rgbabuf[i] );
	}

*texW = W0; *texH = H0;
}

void setfilteropt( int filteropt )
{
glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filteropt );
glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filteropt );
}

// ici il s'agit de chiffres de 0 a 9, sinon rien
void affModel( int i )
{
if	( ( i < 0 ) || ( i > 9 ) )
	return;
/* seulement utile si on enable pas lighting (je suppose) */
glColor4f(1.0f, 1.0f, 1.0f, 1.0f);

glEnable(GL_TEXTURE_2D);
glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
glBindTexture(GL_TEXTURE_2D, texHand[i] );

glEnableClientState( GL_VERTEX_ARRAY );
glEnableClientState( GL_TEXTURE_COORD_ARRAY );

glVertexPointer( 3, GL_FLOAT, 0, xyz3 );
glTexCoordPointer( 2, GL_FLOAT, 0, st2 );
glDrawElements( GL_QUADS, 4, GL_UNSIGNED_INT, vx );

/* IMPORTANT pour permettre le trace d'elements non textures a la
   prochaine frame .. (par exemple le repere )
 */
glDisable(GL_TEXTURE_2D);
}
