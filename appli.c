/* coquille pour explorations openGL sous WGL */


#include <windows.h>                    /* must include this before GL/gl.h */
#include <GL/gl.h>                      /* OpenGL header file */
#include <GL/glu.h>                     /* OpenGL utilities header file */
#include <stdio.h>
#include <math.h>

#include "nb_serial.h"
#include "model.h"

void gasp( char *fmt, ... );  /* fatal error handling */

/* non-MSWin zone = portable code ========================================= */


/* variables globales */

int curw, curh;                 /* dimensions fenetre (client area) */
int setw, seth;			// dimensions desirees pour la fenetre (client area)
int WinW, WinH;                 /* dimensions fenetre (total) */
int TexW, TexH;			/* dim. texture pour resize "pixel acurate"  */
int reSizeFlag=1;
int keepSizeFlag = 1;

float xv, yv;                   /* translation "view" */
float xs, ys;			/* scaling "view" */
float zf;			/* zoom factor */

char dig[6];			// les 6 digits a afficher
int option_verbose = 0;

/* calcul des facteurs d'echelle t.q. 1 texel --> zf pixels */
void updatescales()
{
xs = zf * ( (float)TexW ) / ( (float)curw ); 
ys = zf * ( (float)TexH ) / ( (float)curh );
setw = (int)( zf * 6.4 * (double)TexW );	// 6 digits + 2 intervalles de 0.2*TexW
seth = (int)( zf *       (double)TexH );
}

/* fonction interpretant un resize de la fenetre */

void WinReSize( int W, int H );

void aReshape(int width, int height)
{
// printf("aReshape %d x %d\n", width, height ); fflush(stdout);
/* 2D tout plat */
glMatrixMode(GL_PROJECTION);
glLoadIdentity();
glMatrixMode(GL_MODELVIEW);
glLoadIdentity();
glViewport(0, 0, width, height); /* viewport size in pixels */ 
curw = width; curh = height;
if ( keepSizeFlag )
   updatescales(); 
// N.B. WinReSize() utilise la fonction MoveWindow() de MS qui prend les dimensions
// "cadre compris", or on ne connait pas la largeur du cadre.
// on va donc corriger la fenetre initiale qui �tait seulement approximative
// le but c'est d'avoir une surface UTILE de setw x seth
if ( reSizeFlag == 1 )	
   {
   WinW += ( setw - curw );	// correction
   WinH += ( seth - curh );
   WinReSize( WinW, WinH );
   reSizeFlag = 0;
   }
// printf("xv=%g, yv=%g, xs=%g ys=%g\n", xv, yv, xs, ys ); fflush(stdout);
}


/* fonction affichant le monde selon view et les objets
   selon vobj */

void aDisplay()
{
glClearColor( 0.0f, 0.0f, 0.0f, 1.0 );
glClear(GL_COLOR_BUFFER_BIT);

glMatrixMode(GL_MODELVIEW);
glLoadIdentity();

/* elements en coord normalisees
   i.e. le viewport fait -1,-1 a +1,+1  */

glPushMatrix();
glTranslatef( xv, yv, 0.0f );
glScalef( xs, ys, 1.0f );
affModel( dig[0] );
glTranslatef( 2.0, 0.0, 0.0f );	// la largeur de la texture est 2.0 dans ce repere
affModel( dig[1] );

glTranslatef( 2.4, 0.0, 0.0f );
affModel( dig[2] );
glTranslatef( 2.0, 0.0, 0.0f );
affModel( dig[3] );

glTranslatef( 2.4, 0.0, 0.0f );
affModel( dig[4] );
glTranslatef( 2.0, 0.0, 0.0f );
affModel( dig[5] );

glPopMatrix();

/* elements en coord-ecran */
// rien

glFlush();
/* ici on doit swapper les buffers, mais comme c'est dependant de l'OS
   on le laisse au niveau superieur */
}

/* fonction specifique de l'appli initialisant divers trucs */
 
void aInit()
{
xv = -1.0f; yv = -1.0f;	/* vue initiale */
xs = ys = 1.0f; zf = 1.0f;
initModel( &TexW, &TexH );
updatescales();
}
 
/* fonctions specifiques de l'appli 
   N.B. l'appel a aDisplay est assure ensuite via le message WM_PAINT
   que WindowProc s'envoie a elle meme
 */
void aKey( short int c )
{
switch( c )
    {
    case 'n' : setfilteropt( GL_NEAREST ); break;
    case 'l' : setfilteropt( GL_LINEAR ); break;
    case 'i' :
        {
        char lbuf[64];
        sprintf( lbuf, "%d x %d", curw, curh );
        MessageBox(NULL, lbuf, "taille client area (WM_SIZE)", MB_OK);
        } break;
    case 'r' : if ( ( curw != setw ) || ( curh != seth ) )
		  {
		  /* on fait un premier resize bidon pour etre sur que la
		     fenetre a bien les dimensions WinW et WinH
		     le vrai resize sera fait par aReshape */
                  WinW = setw; WinH = seth;
		  reSizeFlag = 1;
	          WinReSize( WinW, WinH );
                  } break;
    case 'k' : keepSizeFlag ^= 1; break;
    case 'z' : zf *= 2; updatescales(); break;
    case 'o' : zf /= 2; updatescales(); break;
    case 'f' : zf = 1.0; updatescales(); break;
    case '+' :	if	( dig[0] < 9 )
			++dig[0];
		else 	dig[0] = 0;
    case 'v' : option_verbose ^= 1; break;
    default  : xv = yv = -1.0;
    }
}  

/* system dependant area : serial port / win32 */



// global things for us
char rxbuf[64];
int rxcnt=69;

#define QFIFO		(1<<8)
#define FIFOMASK	(QFIFO-1)
char fifobuf[QFIFO] = {0};
int fifoWI=0, fifoRI=0;
char tmp_dig[6];	// valeurs temporaires pour dig[]

#define STM32_MSG_END	0x0D

// callback pour le timer
void aTime()
{
int i, j, backcnt; char c;
// copier les nouvelles donnees dans le FIFO
rxcnt = nb_serial_read( rxbuf, sizeof(rxbuf)-1 );
if	( rxcnt > 0 )
	{
	for	( i = 0; i < rxcnt; ++i )
		{
		fifobuf[fifoWI++] = rxbuf[i];
		fifoWI &= FIFOMASK;
		}
	// extraire le plus r�cent message du FIFO
	i = fifoWI; j = 0; backcnt = 0;
	while	( ( j < 7 ) && ( backcnt < 25 ) )
		{			// parcourir en arriere, meme s'il faut depasser fifori
		++backcnt;
		i = (i-1) & FIFOMASK;
		c = fifobuf[i];
		if	( ( j == 0 ) && ( c == STM32_MSG_END ) )
			j++;
		else if	( j )
			{
			switch	( j )
				{
				case 1: if	( isdigit(c) )
						{ ++j; tmp_dig[5] = c - '0'; }
					else if ( c != ':' )
						j = 0;
					break;
				case 2: if	( isdigit(c) )
						{ ++j; tmp_dig[4] = c - '0'; }
					else	j = 0;
					break;
				case 3:	if	( isdigit(c) )
						{ ++j; tmp_dig[3] = c - '0'; }
					else if ( c != ':' )
						j = 0;
					break;
				case 4: if	( isdigit(c) )
						{ ++j; tmp_dig[2] = c - '0'; }
					else	j = 0;
					break;
				case 5:	if	( isdigit(c) )
						{ ++j; tmp_dig[1] = c - '0'; }
					else if ( c != ':' )
						j = 0;
					break;
				case 6: if	( isdigit(c) )
						{
						++j; tmp_dig[0] = c - '0';
						dig[0] = tmp_dig[0]; dig[1] = tmp_dig[1]; dig[2] = tmp_dig[2];
						dig[3] = tmp_dig[3]; dig[4] = tmp_dig[4]; dig[5] = tmp_dig[5];
						}
					else	j = 0;
					break;
				}
			}
		}
	fifoRI = fifoWI;
	// verbose
	if	( option_verbose )
		{
		for	( i = 0; i < rxcnt; ++i )
			if	( rxbuf[i] < ' ' )
				rxbuf[i] = ' ';
		rxbuf[(rxcnt<(int)sizeof(rxbuf))?(rxcnt):(sizeof(rxbuf)-1)] = 0;
		printf("%5d new bytes: [%s] <-%d\n", rxcnt, rxbuf, backcnt ); fflush(stdout);
		}
	}
}

/* system dependant area Numero 1 : peu dependante de l'appli
   utilise des fonctions Win32 specialement faites pour opengl...
 */

HDC   hDC;              /* hDC = device context declare en global */
HGLRC hRC;              /* hRC = opengl context declare en global */
HWND  hWnd;             /* hWnd = window handle declare en global */

/* preparation format pixel */  
void preparePixel( HWND hWnd )
{
int         pf;
PIXELFORMATDESCRIPTOR pfd;             

hDC = GetDC(hWnd);   /* hDC = device context declare en global */

/* on met d'abord tous les bits a zero, car peu seront non nuls */
memset(&pfd, 0, sizeof(pfd));
pfd.nSize        = sizeof(pfd);
pfd.nVersion     = 1;
pfd.dwFlags      = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
pfd.iPixelType   = PFD_TYPE_RGBA;
pfd.cDepthBits   = 32;
pfd.cColorBits   = 32;

pf = ChoosePixelFormat(hDC, &pfd);
if	(pf == 0) {
	gasp("ChoosePixelFormat() failed"); 
	} 

if	(SetPixelFormat(hDC, pf, &pfd) == FALSE) {
	gasp("SetPixelFormat() failed");
	} 

DescribePixelFormat(hDC, pf, sizeof(PIXELFORMATDESCRIPTOR), &pfd);

hRC = wglCreateContext(hDC);  /* fonction wgl hybrides Win32/opengl */
wglMakeCurrent(hDC, hRC);
}

void largueContexts( HWND hWnd )
{
wglMakeCurrent(NULL, NULL);   /* fonction wgl hybrides Win32/opengl */
ReleaseDC(hWnd, hDC);
wglDeleteContext(hRC);
}


/* system dependant area Numero 1 : peu dependante de l'appli
   utilise des fonctions Win32 ordinaires, presque rien de propre a opengl...
 */

/* gasp pour windows */
void gasp( char *fmt, ... )  /* fatal error handling */
{
char lbuf[1024];
va_list  argptr;
va_start( argptr, fmt );
vsprintf( lbuf, fmt, argptr );
va_end( argptr );
MessageBox(hWnd, lbuf, "HORREUR", MB_OK);
exit(1);
}

/* resize autoritaire de la fenetre */

void WinReSize( int W, int H )
{
MoveWindow(
	hWnd,  // handle of window
	0,      // horizontal position
	0,      // vertical position
	WinW, // width
	WinH,        // height
	TRUE       // repaint flag
	);
}

/* la fonction qui interprete les messages. 
   elle traite ceux qui sont independants de l'application et appelle
   aDisplay, aReshape, aKey(), aTime() pour les autres
 */
   
LONG WINAPI WindowProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{ 
static PAINTSTRUCT ps;

switch(uMsg) {
	case WM_PAINT:
		aDisplay(); SwapBuffers(hDC); /* hDC = device context declare en global */
		BeginPaint(hWnd, &ps);
		EndPaint(hWnd, &ps);
		return 0;
	
	case WM_SIZE:
		aReshape(LOWORD(lParam), HIWORD(lParam));
		PostMessage(hWnd, WM_PAINT, 0, 0);
		return 0;
	
	case WM_TIMER:
		aTime();
		PostMessage(hWnd, WM_PAINT, 0, 0);
		return 0;
	
	case WM_CHAR:
		switch (wParam) {
			case 27 : PostQuitMessage(0); break;
			default : aKey( (short int) wParam );
				      PostMessage(hWnd, WM_PAINT, 0, 0);
			}
		return 0;
	
	case WM_CLOSE:
	    PostQuitMessage(0);
	    return 0;
	}

return DefWindowProc(hWnd, uMsg, wParam, lParam); 
} 

/* creation fenetre Win32 tout a fait ordinaire */

HWND CreateMyWindow(char* title, int x, int y, int width, int height )
{
static HINSTANCE hInstance = 0;

/* only register the window class once - use hInstance as a flag. */
if	(!hInstance)
	{
	WNDCLASS wc;
	hInstance = GetModuleHandle(NULL);
	wc.style         = CS_OWNDC;
	wc.lpfnWndProc   = (WNDPROC)WindowProc;
	wc.cbClsExtra    = 0;
	wc.cbWndExtra    = 0;
	wc.hInstance     = hInstance;
	wc.hIcon         = LoadIcon(NULL, IDI_WINLOGO);
	wc.hCursor       = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = NULL;
	wc.lpszMenuName  = NULL;
	wc.lpszClassName = "myClass";
	
	if	(!RegisterClass(&wc))
		{
		MessageBox(NULL, "RegisterClass() failed: Cannot register window class.", "Error", MB_OK);
		return NULL;
		}
	}

hWnd = CreateWindow( "myClass", title, WS_OVERLAPPEDWINDOW | WS_CLIPSIBLINGS | WS_CLIPCHILDREN,
	x, y, width, height, NULL, NULL, hInstance, NULL );
return hWnd;
}

/* la fonction main a la Win32
   "To convert the command line to an argv style array of strings,
    call the CommandLineToArgv function."
 */
int APIENTRY WinMain(HINSTANCE hCurrentInst, HINSTANCE hPreviousInst,
                      LPSTR lpszCmdLine, int nCmdShow)
{
HWND  hWnd;                         /* window */
MSG   msg;                          /* message */

WinW = 170 * ( 6 + 0.4 ) + 50; WinH = 170 + 50;	// 6 digits nixie de 170*170
						// les marges seront ajustees automatiquement

hWnd = CreateMyWindow("Nixie - Double Buffer - True Color", 0, 0, WinW, WinH );
if	( hWnd == NULL )
	gasp("failed CreateMyWindow");

preparePixel( hWnd);

aInit();

int portnum;
portnum = lpszCmdLine[0];
if	( isdigit( portnum ) )
	{
	printf("arg %c\n", portnum ); fflush(stdout);
	portnum -= '0';
	}
else	portnum = 0;

#define MY_BAUD_RATE 19200
portnum = nb_open_serial_ro( portnum, MY_BAUD_RATE );
if	( portnum >=1 )
	{ printf("ouverture COM%d a %d bauds Ok\n", portnum, MY_BAUD_RATE ); fflush(stdout);  }
else	gasp("failed open port COM, code %d", portnum );
 
#define PERIODE_MS 31
SetTimer( hWnd, 0, PERIODE_MS, NULL );	// pas de call-back, on compte sur un message

ShowWindow(hWnd, nCmdShow);
    
/* boucle principale de Windows */

while	(GetMessage(&msg, hWnd, 0, 0))
	{
	TranslateMessage(&msg);
	DispatchMessage(&msg);
	}

largueContexts(hWnd);

DestroyWindow(hWnd);
return msg.wParam;
}
